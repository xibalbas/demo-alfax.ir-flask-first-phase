# ALFAX.ir DEMO

alfax is a personal Project for Practicing Django and Flask !

## Installation

Use the package manager [git](https://github.com) to Downlaod Project
.

```bash
git@gitlab.com:jupidev-code/demo-alfax.ir-flask-first-phase.git
```
Or

```bash
git clone https://gitlab.com/jupidev-code/demo-alfax.ir-flask-first-phase.git
```

## Usage


```bash
cd demo-alfax.ir-flask-first-phase
pip install -r requirements.txt
python app.py
```


## TODO

[] Add To Do
[] Add Update DB