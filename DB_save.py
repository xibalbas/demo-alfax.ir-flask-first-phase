import os
import pandas as pd
import sqlite3


conn = sqlite3.connect('cataloge.db')
c = conn.cursor()
c.execute('CREATE TABLE IF NOT EXISTS shaft(lenght,weight,bar,type)')
c.execute('CREATE TABLE IF NOT EXISTS pomp(pompnum,debi,shafttype,pomptype)')
c.execute('CREATE TABLE IF NOT EXISTS test(test1,test2,test3,test4)')
c.execute('''CREATE TABLE IF NOT EXISTS factors(customer_name,
                                                project_name,
                                                address,
                                                systemtype,
                                                travel,
                                                w1,
                                                w2,
                                                jack,
                                                power,
                                                carsling,
                                                oil,
                                                pipe,
                                                rapcher,
                                                handpomp,
                                                shafttool,
                                                shaftmodel,
                                                pomp,
                                                motor,
                                                date,
                                                val,
                                                totalsode,
                                                jacksode,
                                                powersode,
                                                carsode,
                                                ourprofit)''')

            
conn.commit()


def xls_to_csv (conn):
    path = str(os.getcwd())
    datashaft = path +"\\data.xlsx"
    datapomp = path +"\\datapomp.xlsx"
    price = path +"\\pricing.xlsx"

    if os.path.exists(datashaft) and os.path.exists(datapomp) and os.path.exists(price):
        df = pd.read_excel(datashaft)
        ds1 = pd.DataFrame(df)
        df = pd.read_excel(datapomp)
        ds2 = pd.DataFrame(df)
        df = pd.read_excel(price)
        ds3 = pd.DataFrame(df)
        ds1.to_sql('shaft', conn, if_exists='replace', index = False)
        ds2.to_sql('pomp', conn, if_exists='replace', index = False)
        ds3.to_sql('price', conn, if_exists='replace', index = False)

xls_to_csv (conn)
conn.commit()